#include <LiquidCrystal.h>

// Pin definitions
#define pHpin A0
#define VREF 5.0
#define R1 10000.0
#define OFFSET 0.00

// Initialize the LCD
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  Serial.begin(9600); // Initialize serial communication
  lcd.begin(16, 2); // Initialize the LCD
}

void loop() {
  float pH, voltage;
  pH = readpH(); // Read the pH value
  voltage = readVoltage(); // Read the voltage value
  lcd.clear(); // Clear the LCD
  lcd.print("pH: ");
  lcd.print(pH, 2); // Display the pH value with 2 decimal places
  lcd.setCursor(0, 1);
  lcd.print("Voltage: ");
  lcd.print(voltage, 2); // Display the voltage value with 2 decimal places
  delay(1000); // Wait for 1 second
}

float readpH() {
  float sum = 0;
  for(int i=0; i<10; i++) { // Take 10 measurements and average them
    int sensorValue = analogRead(pHpin);
    sum += sensorValue;
  }
  float pH = (sum/10.0)*VREF/1023.0; // Convert the sensor value to pH
  pH = 3.5*pH + OFFSET; // Apply calibration
  return pH;
}

float readVoltage() {
  float sensorValue = analogRead(pHpin);
  float voltage = sensorValue*VREF/1023.0;
  return voltage;
}



/*
En este código, la función readpH() toma 10 mediciones del valor del sensor de pH y las promedia. Luego, convierte el valor promedio a unidades de pH y aplica una corrección de calibración. La función readVoltage() simplemente lee el valor del sensor y lo convierte a voltios. En el loop principal, se muestra el valor de pH y voltaje en la pantalla LCD.

Recuerda que es importante calibrar el medidor de pH antes de usarlo. Para hacerlo, sigue las instrucciones del fabricante del módulo de sensor de pH. Generalmente, se necesitan soluciones tampón con un pH conocido para calibrar el medidor de pH.
*/





